﻿using NUnit.Framework;
using pankkisovellus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pankkisovellusTests
{
    [TestFixture] //attribuutti, kertoo että kyseessä testiluokka
    public class PankkitiliTest
    {

        /// <summary>
        /// pankkitili Sovellus sisältää:
        /// 
        /// luokka, jonka nimi on Pankkitili
        /// Pankkitilissä on 3 toimintoa
        /// 
        /// -tallettaa rahaa
        /// - nostaa rahaa (ei saa mennä miinukselle)
        /// - siirtää rahaa tililtä toiselle (ei saa mennä miinukselle)
        /// 
        /// </summary>
        /// 


        public Pankkitili tili1;

        // OneTimeSetUp ja OneTimeTearDown
        //setup aina alussa sama tilanne, teardown tyhjentää


        [SetUp]
        public void TestienAlustaja()
        {
            this.tili1 = new Pankkitili(100);
        }

        [Test]
        public void LuoPankkitili()
        {
            


            // testataan olion luokan tyyppi
            Assert.IsInstanceOf<Pankkitili>(tili1);
        }

        [Test]

        public void AsetaPankkitilillealkusaldo()
        {
           

            Assert.That(100, Is.EqualTo(tili1.Saldo));
        }

        [Test]
        public void Talletarahaapankkitilille()
        {
        

        tili1.Talleta(250);
        Assert.That(350, Is.EqualTo(tili1.Saldo));

        }
        
        [Test]
        public void NostaRahaaTililta()
        {
            tili1.NostaRahaa(75);

            Assert.That(25, Is.EqualTo(tili1.Saldo));
        }

        [Test]
        public void NostonJalkeenPankkitiliEiMiinuksella ()
        {
            
            //antaako ohjelma halutun virheentyypin
            Assert.Throws<ArgumentException>(() => tili1.NostaRahaa(175));
             //rahoja ei menetetä, sama kuin loppusaldo
            Assert.That(100, Is.EqualTo(tili1.Saldo));
        }
    }


}