﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pankkisovellus
{
    public class Pankkitili
    {
        public Pankkitili(int alkusaldo)//luokan rakentaja
        {
            Saldo = alkusaldo;
        }

        public int Saldo { get; set; } //metodeja

        public void Talleta(int maara)
        {
            Saldo += maara;
        }

        public void NostaRahaa(int maara)
        {
        
        
        if (Saldo - maara < 0) 
        {
            throw new ArgumentException("Tilillä ei rahaa.");
        }
                                             
            Saldo -= maara;
                                             
        }

    }
}
