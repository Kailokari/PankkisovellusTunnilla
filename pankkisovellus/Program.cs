﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pankkisovellus
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Pankkisovellus");

            Console.WriteLine("aNNA ALKUSALDON MÄÄRÄ");
            string alkusaldo = Console.ReadLine();

            Pankkitili pankkitili1 = new Pankkitili(int.Parse(alkusaldo));

            Console.WriteLine("Tilin 01 alkusaldo on: " + pankkitili1.Saldo );

            Console.WriteLine("Annan nostettava MÄÄRÄ: ");
            string rahamaara = Console.ReadLine();

            try
            {
                pankkitili1.NostaRahaa(int.Parse(rahamaara));

                Console.WriteLine("tgilin alkusaldo on: " + pankkitili1.Saldo);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Tilillä ei ole rahaa.");
            }
        }
    }
}
